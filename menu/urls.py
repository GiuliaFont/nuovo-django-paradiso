from django.urls import path

from menu.views import FoodCategoryListView, FoodCategoryDetailView, FoodCategoryCreateView, FoodCategoryUpdateView


app_name = "menu"

urlpatterns = [
    path(
    "categories/",
    FoodCategoryListView.as_view(),
    name = "categories_list"
    ),

    path(
    "category/<int:pk>/",
    FoodCategoryDetailView.as_view(),
    name = "category_detail"
    ),

    path(
    "create/category",
    FoodCategoryCreateView.as_view(),
    name = "create_category"
    ),

    path(
    "update/<int:pk>/category",
    FoodCategoryUpdateView.as_view(),
    name = "update_category"
    )
]