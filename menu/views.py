from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django.urls import reverse_lazy

from menu.models import FoodCategory


__all__ = [
    "FoodCategoryCreateView",
    "FoodCategoryDetailView",
    "FoodCategoryListView"
]

class FoodCategoryListView(ListView):
    model = FoodCategory


class FoodCategoryDetailView(DetailView):
    model = FoodCategory


class FoodCategoryCreateView(CreateView):
    model = FoodCategory
    fields = ("name", "description")

    success_url = reverse_lazy("menu:categories_list")

class FoodCategoryUpdateView(UpdateView):
     model = FoodCategory
     fields = ("name", "description")

     success_url = reverse_lazy("menu:categories_list")
