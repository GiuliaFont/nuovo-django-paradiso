from django.db import models

class FoodCategory(models.Model):

    name = models.CharField(max_length=255)
    description = models.TextField()


class Dish(models.Model):

    name = models.CharField(max_length=255)
    description = models.TextField()

    category = models.ForeignKey(FoodCategory, on_delete=models.PROTECT)
    