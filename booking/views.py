from django.shortcuts import render, redirect
from datetime import datetime, timedelta
from .models import *
from django.contrib import messages


def home(request):
    return render(request, "home.html", {})


def booking(request):
    #calling "validWeekday" Function to loop days you want in the next 21 days:
    weekdays = validWeekday(22)

    validateWeekdays = isWeekdayValid(weekdays)


    if request.method == "POST":
        service = request.POST.get("service")
        day = request.POST.get("day")
        if service == None:
            messages.success(request, "Please Select a Service!")
            return redirect("booking")
        
        request.session["day"] = day
        request.session["service"] = service

        return redirect("bookingSubmit")
    

    return render(request, "booking.html", {
            "weekdays" : weekdays,
            "validateWeekdayes" : validateWeekdays,
        })


def bookingSubmit(request):
    user = request.user
    times = [
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20",
        "21",
        "22"
    ]
    today = datetime.now()
    minDate = today.strftime("%d-%m-%Y")
    deltatime = today + timedelta(days = 21)
    strdeltatime = deltatime.strftime("%d-%m-%Y")
    maxDate = strdeltatime

    day = request.session.get("day")
    service = request.session.get("service")

    hour = checkTime(times, day)
    if request.method == "POST":
        time = request.POST.get("time")
        date = dayToWeekday(day)

        if service != None:
            if day <= maxDate and day >= minDate:
                if date == "Monday" or date == "Saturday" or date == "Wednesday":
                    if Reservation.objects.filter(day=day).count() < 11:
                        if Reservation.objects.filter(day=day, time=time).count() < 1:
                            ReservationForm = Reservation.objects.get_or_create(
                                user = user,
                                service = service,
                                day = day,
                                time = time,
                            )
                            messages.success(request, "Reservation Saved!")
                            return redirect("home")
                        else:
                            messages.success(request, "The selected time has been resrved before")
                    else:
                        messages.success(request, "the selected day is full!")
                else:
                    messages.success(request, "the selected date is incorrect!")
            else:
                messages.success(request, "The selected date isn't in the correct time period")
        else:
            messages.success(request, "please select a service!")

    
    return render(request, "bookingSubmit.html", {"times" : hour})


def userPanel(request):
    user = request.user
    reservations = Reservation.objects.filter(user=user).order_by("day","time")
    return render(request, "userPanel.html", {
        "user" : user,
        "reservations" : reservations,
    })

def userUpdate(request, id):
    reservation = Reservation.objects.get(pk=id)
    userdatepicked = reservation.day
    today = datetime.today()
    minDate = today.strftime("%d-%m-%Y")


    delta24 = (userdatepicked).strftime("%d-%m-%Y") >= (today + timedelta(days=1)).strftime("%d-%m-%Y")
    weekdays = validWeekday(22)

    validateWeekdays = isWeekdayValid(weekdays)


    if request.method == "POST":
        service = request.POST.get("service")
        day = request.POST.get("day")

        request.session["day"] = day
        request.session["service"] = service

        return redirect("userUpdateSubmit", id=id)
    

    return render(request, "userUpdate.html", {
        "weekdays" : weeekdays,
        "validateWeekdays" : validateWeekdays,
        "delta24": delta24,
        "id" : id,
    })


def UserUpdateSubmit(request, id):
     user = request.user
     times = [
         "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
        "20",
        "21",
        "22"]
     today = datetime.now()
     minDate = today.strftime('%Y-%m-%d')
     deltatime = today + timedelta(days=21)
     strdeltatime = deltatime.strftime('%Y-%m-%d')
     maxDate = strdeltatime

     day = request.session.get("day")
     service = request.session.get("service")

     hour = checkEditTime(times, day, id)
     reservation = Reservation.objects.get(pk = id)
     userSelectedTime = reservation.time
     if request.method == 'POST':
        time = request.POST.get("time")
        date = dayToWeekday(day)

        if service != None:
            if day <= maxDate and day >= minDate:
                if date == 'Monday' or date == 'Saturday' or date == 'Wednesday':
                    if Reservation.objects.filter(day=day).count() < 11:
                        if Reservation.objects.filter(day=day, time=time).count() < 1 or userSelectedTime == time:
                            ReservationForm = Reservation.objects.filter(pk=id).update(
                                user = user,
                                service = service,
                                day = day,
                                time = time,
                            ) 
                            messages.success(request, "Reservation Edited!")
                            return redirect('home')
                        else:
                            messages.success(request, "The Selected Time Has Been Reserved Before!")
                    else:
                        messages.success(request, "The Selected Day Is Full!")
                else:
                    messages.success(request, "The Selected Date Is Incorrect")
            else:
                    messages.success(request, "The Selected Date Isn't In The Correct Time Period!")
        else:
            messages.success(request, "Please Select A Service!")
        return redirect('userPanel')


     return render(request, 'userUpdateSubmit.html', {
        'times':hour,
        'id': id,
    })


def staffPanel(request):
   today = datetime.today()
   minDate = today.strftime('%Y-%m-%d')
   deltatime = today + timedelta(days=21)
   strdeltatime = deltatime.strftime('%Y-%m-%d')
   maxDate = strdeltatime
    #Only show the Appointments 21 days from today
   items = Reservation.objects.filter(day__range=[minDate, maxDate]).order_by('day', 'time')
   
   return render(request, 'staffPanel.html', {
        'items':items,
    }) 


def dayToWeekday(x):
    z = datetime.strptime(x, "%Y-%m-%d")
    y = z.strftime('%A')
    return y


def validWeekday(days):
    #Loop days you want in the next 21 days:
    today = datetime.now()
    weekdays = []
    for i in range (0, days):
        x = today + timedelta(days=i)
        y = x.strftime('%A')
        if y == 'Monday' or y == 'Saturday' or y == 'Wednesday':
            weekdays.append(x.strftime('%Y-%m-%d'))
    return weekdays


def isWeekdayValid(x):
    validateWeekdays = []
    for j in x:
        if Reservation.objects.filter(day=j).count() < 10:
            validateWeekdays.append(j)
    return validateWeekdays


def checkTime(times, day):
    #Only show the time of the day that has not been selected before:
    x = []
    for k in times:
        if Reservation.objects.filter(day=day, time=k).count() < 1:
            x.append(k)
    return x


def checkEditTime(times, day, id):
    #Only show the time of the day that has not been selected before:
    x = []
    appointment = Reservation.objects.get(pk=id)
    time = appointment.time
    for k in times:
        if Reservation.objects.filter(day=day, time=k).count() < 1 or time == k:
            x.append(k)
    return x