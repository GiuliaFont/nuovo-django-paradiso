from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
# Create your models here.

SERVICE_CHOICES = (
    ("Breakfast", "Breakfast"),
    ("Brunch", "Brunch"),
    ("Lunch", "Lunch"),
    ("Teatime", "Teatime"),
    ("Happy Hour", "Happy Hour"),
    ("Dinner", "Dinner")
)
TIME_CHOICES = (
    ("8", "8"),
    ("9", "9"),
    ("10", "10"),
    ("11", "11"),
    ("12", "12"),
    ("13", "13"),
     ("14", "14"),
    ("15", "15"),
    ("16", "16"),
    ("17", "17"),
    ("18", "18"),
    ("19", "19"),
    ("20", "20"),
    ("21", "21"),
    ("22", "22")
)

class Reservation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, 
                             null = True, blank = True)
    service = models.CharField(max_length=50, 
                               choices=SERVICE_CHOICES, 
                               default="Lunch")
    day = models.DateField(default=datetime.now)
    time = models.CharField(max_length = 10, 
                            choices = TIME_CHOICES, 
                            default = "3 PM")
    time_ordered = models.DateTimeField(default=datetime.now, 
                                        blank = True)
    
    def __str__(self):
        return f"{self.user.username}| day: {self.day} | time: {self.time}"
    
